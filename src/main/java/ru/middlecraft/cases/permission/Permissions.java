package ru.middlecraft.cases.permission;

/**
 * Created by Sceri 24.06.2016.
 */
public class Permissions {
    public static final String CASE_COMMAND = "middlecases.command.case";
    public static final String CASE_HELP_COMMAND = "middlecases.command.help";
    public static final String CASE_LIST_COMMAND = "middlecases.command.list";
    public static final String CASE_OPEN_COMMAND = "middlecases.command.open";
    public static final String CASE_GET_COMMAND = "middlecases.command.get";
    public static final String CASE_INFO_COMMAND = "middlecases.command.info";
    public static final String CASE_ADD_COMMAND = "middlecases.command.add";
}
