package ru.middlecraft.cases.permission;

/**
 * Created by Sceri 24.06.2016.
 */
public class PermissionException extends Exception {

    public PermissionException() {
        super();
    }

    public PermissionException(String message) {
        super(message);
    }

    public PermissionException(String message, Throwable cause) {
        super(message, cause);
    }

    public PermissionException(Throwable cause) {
        super(cause);
    }

}
