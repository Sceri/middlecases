package ru.middlecraft.cases;

import com.google.common.base.Charsets;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.java.JavaPlugin;
import ru.middlecraft.cases.command.Command;
import ru.middlecraft.cases.command.CommandArgs;
import ru.middlecraft.cases.command.CommandException;
import ru.middlecraft.cases.command.CommandManager;
import ru.middlecraft.cases.database.CaseRecord;
import ru.middlecraft.cases.database.DatabaseConfig;
import ru.middlecraft.cases.database.DatabaseManager;
import ru.middlecraft.cases.inventory.Case;
import ru.middlecraft.cases.inventory.Case.CaseAdapter;
import ru.middlecraft.cases.inventory.item.CaseItem;
import ru.middlecraft.cases.inventory.item.CaseItem.CaseItemAdapter;
import ru.middlecraft.cases.inventory.item.reward.CaseReward;
import ru.middlecraft.cases.inventory.item.reward.CaseReward.CaseRewardAdapter;
import ru.middlecraft.cases.task.DatabaseTask;
import ru.middlecraft.cases.task.TaskManager;
import ru.middlecraft.cases.util.json.ItemStackAdapter;
import ru.middlecraft.cases.util.json.MaterialDataAdapter;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static ru.middlecraft.cases.database.CaseRecord.*;
import static ru.middlecraft.cases.permission.Permissions.*;
import static ru.middlecraft.cases.util.BukkitUtils.*;
import static org.bukkit.ChatColor.*;

/**
 * Created by Sceri 20.06.2016.
 */
public class MiddleCases extends JavaPlugin {

    private static MiddleCases INSTANCE;

    private TaskManager taskManager = new TaskManager();
    private DatabaseManager databaseManager;
    private CommandManager commandManager;
    private static final Gson GSON = new GsonBuilder().
            registerTypeHierarchyAdapter(CaseReward.class, new CaseRewardAdapter()).
            registerTypeAdapter(ItemStack.class, new ItemStackAdapter()).
            registerTypeAdapter(Case.class, new CaseAdapter()).
            registerTypeAdapter(CaseItem.class, new CaseItemAdapter()).
            registerTypeAdapter(MaterialData.class, new MaterialDataAdapter()).
            setPrettyPrinting().
            create();
    private static final Type CASES_TYPE = new TypeToken<List<Case>>() {}.getType();
    private Map<String, Case> caseMap;

    @Override
    public void onLoad() {
        INSTANCE = this;
    }

    @Override
    public void onEnable() {
        initDatabase();
        initCases();

        commandManager = new CommandManager(this);
        commandManager.registerCommands(this);
        commandManager.registerHelp();
    }

    @Override
    public void onDisable() {

    }

    @Command(name = "case", permission = CASE_COMMAND)
    public void caseCommand(CommandArgs args) {
        showHelp(args.getPlayer());
    }

    @Command(name = "case.help", permission = CASE_HELP_COMMAND)
    public void caseHelp(CommandArgs args) {
        showHelp(args.getPlayer());
    }

    @Command(name = "case.add", permission = CASE_ADD_COMMAND)
    public void caseAdd(final CommandArgs args) throws CommandException {
        if(args.length() >= 2) {
            final String caseName = args.getArgs(0);
            final String playerName = args.getArgs(1);

            getTaskManager().execute(new DatabaseTask(args.getSender()) {
                @Override
                public void process() throws SQLException {
                    getCaseRecords().create(new CaseRecord(caseName, playerName));
                    syncSendMessage(args.getSender(), GREEN + "Запись успешно добавлена в БД");
                }
            });
            return;
        }

        throw new CommandException(RED + "Недостаточно аргументов");
    }

    @Command(name = "case.list", permission = CASE_LIST_COMMAND)
    public void caseList(CommandArgs args) {
        args.getSender().sendMessage(AQUA + "Список доступных кейсов: " + Joiner.on(", ").join(caseMap.keySet()));
    }

    @Command(name = "case.open", permission = CASE_OPEN_COMMAND, inGameOnly = true)
    public void caseOpen(CommandArgs args) throws CommandException {
        if(args.length() == 0) {
            if(caseMap.size() == 1) {
                Iterables.getFirst(caseMap.values(), null).openCase(args.getPlayer());
                return;
            }

            caseList(args);

            throw new CommandException(RED + "Необходимо указать имя кейса");
        }

        String caseName = args.getArgs(0);
        if(caseMap.containsKey(caseName)) {
            caseMap.get(caseName).openCase(args.getPlayer());
            return;
        }

        throw new CommandException(RED + "Неизвестное имя кейса");
    }

    @Command(name = "case.get", permission = CASE_GET_COMMAND, inGameOnly = true)
    public void caseGet(final CommandArgs args) {
        if(args.length() == 0) {
            getTaskManager().execute(new DatabaseTask(args.getSender()) {
                @Override
                public void process() throws SQLException {
                    List<CaseRecord> records = getCaseRecords().queryBuilder().
                            where().
                            eq(CASE_PLAYER_COLUMN, args.getPlayer().getName()).
                            and().
                            isNotNull(CASE_OPEN_TIME_COLUMN).
                            and().
                            isNull(CASE_PROCESS_TIME_COLUMN).
                            query();

                    if(!records.isEmpty()) {
                        final List<String> lines = Lists.newArrayList();

                        lines.add(AQUA + "Список доступных предметов из кейсов:");
                        for(CaseRecord record : records) {
                            CaseItem item = caseMap.get(record.getName()).getItem(record.getRewardId());
                            String itemName = item.getItemStack().getItemMeta().getDisplayName();

                            lines.add(String.format("%s - %s: %s (%s)", GREEN, AQUA + String.valueOf(record.getId()) + WHITE, WHITE + itemName + WHITE, AQUA + String.valueOf(item.getReward().getType()) + WHITE));
                        }

                        syncExecuteQuietly(new Runnable() {
                            @Override
                            public void run() {
                                for(String line : lines) {
                                    args.getSender().sendMessage(line);
                                }
                            }
                        });
                    } else {
                        syncSendMessage(args.getSender(), RED + "Нет доступных предметов");
                    }
                }
            });
            return;
        }

        final int id = Integer.parseInt(args.getArgs(0));
        getTaskManager().execute(new DatabaseTask(args.getSender()) {
            @Override
            public void process() throws SQLException {
                CaseRecord record = getCaseRecords().queryForId(id);

                if(record == null) {
                    syncSendMessage(args.getSender(), RED + "Неизвестный ID записи");
                    return;
                }

                if(caseMap.containsKey(record.getName())) {
                    final CaseItem caseItem = caseMap.get(record.getName()).getItem(record.getRewardId());

                    record.setProcessed();
                    getCaseRecords().update(record);

                    try {
                        syncExecute(new Runnable() {
                            @Override
                            public void run() {
                                caseItem.getReward().process(args.getPlayer());
                                args.getPlayer().sendMessage(GREEN + "Награда успешно выдана");
                            }
                        });
                    } catch (Exception e) {
                        syncSendMessage(args.getSender(), RED + "Не удалось выдать предмет");
                    }
                }
            }
        });
    }

    @Command(name = "case.items")
    public void caseItems(final CommandArgs args) throws CommandException {
        if(args.length() == 0) {
            if(caseMap.size() == 1) {
                showItemList(args.getSender(), Iterables.getFirst(caseMap.values(), null));
                return;
            }

            caseList(args);

            throw new CommandException(RED + "Необходимо ввести имя кейса");
        }

        String caseName = args.getArgs(0);
        if(caseMap.containsKey(caseName)) {
            showItemList(args.getSender(), caseMap.get(caseName));
            return;
        }

        throw new CommandException(RED + "Неизвестное имя кейса");
    }

    private void showItemList(CommandSender sender, Case aCase) {
        List<String> lines = Lists.newArrayList();

        lines.add(AQUA + "Список предметов в кейсе:");
        for(CaseItem item : aCase.getItemsList()) {
            String itemName = item.getItemStack().getItemMeta().getDisplayName();
            lines.add(String.format("%s - %s (%s)", GREEN, WHITE + itemName + WHITE, AQUA + item.getReward().getType().toString() + WHITE));
        }

        for (String line : lines) {
            sender.sendMessage(line);
        }
    }

    private void showHelp(Player player) {
        player.sendMessage(GOLD + "- /case (Главная команда плагина)");
        player.sendMessage(GOLD + "- /case help (Получить список доступных команд)");
        player.sendMessage(GOLD + "- /case list (Получить список доступных для открытия кейсов)");
        player.sendMessage(GOLD + "- /case open [case] (Открыть кейс)");
        player.sendMessage(GOLD + "- /case get [id] (Получить награду из открытого кейса)");
        player.sendMessage(GOLD + "- /case add [case] [player] (Добавить кейс игроку)");
    }

    private void initDatabase() {
        File databaseConfig = new File(getDataFolder(), "database.json");
        if(databaseConfig.exists()) {
            try {
                DatabaseConfig config = GSON.fromJson(Files.toString(databaseConfig, Charsets.UTF_8), DatabaseConfig.class);
                databaseManager = new DatabaseManager(config);
            } catch (JsonParseException ex) {
                getLogger().log(Level.WARNING, "Error while parsing database.json, please check syntax", ex);
            } catch (IOException e) {
                getLogger().log(Level.WARNING, "Error while reading database.json", e);
            } catch (SQLException e) {
                getLogger().log(Level.WARNING, "BD SLOMANA", e);
            }
        }
    }

    private void initCases() {
        File casesFile = new File(getDataFolder(), "cases.json");
        if(casesFile.exists()) {
            try {
                List<Case> cases = GSON.fromJson(Files.toString(casesFile, Charsets.UTF_8), CASES_TYPE);
                this.caseMap = Maps.uniqueIndex(cases, new Function<Case, String>() {
                    @Override
                    public String apply(Case aCase) {
                        if(aCase != null) {
                            return aCase.getName();
                        }
                        return null;
                    }
                });
            } catch (JsonParseException ex) {
                getLogger().log(Level.WARNING, "Error while parsing cases.json, please check syntax", ex);
            } catch (IOException e) {
                getLogger().log(Level.WARNING, "Error while reading cases.json", e);
            }
        }
    }

    public TaskManager getTaskManager() {
        return taskManager;
    }

    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    public static MiddleCases getInstance() {
        return INSTANCE;
    }

    public static Logger logger() {
        return getInstance().getLogger();
    }

    public static TaskManager task() {
        return getInstance().getTaskManager();
    }

}
