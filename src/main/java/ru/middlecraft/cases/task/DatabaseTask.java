package ru.middlecraft.cases.task;

import com.j256.ormlite.dao.Dao;
import org.bukkit.command.CommandSender;
import ru.middlecraft.cases.MiddleCases;
import ru.middlecraft.cases.database.CaseRecord;
import ru.middlecraft.cases.permission.PermissionException;

import java.sql.SQLException;

/**
 * Created by Sceri 24.06.2016.
 */
public abstract class DatabaseTask extends Task {
    private final Dao<CaseRecord, Integer> caseRecords = MiddleCases.getInstance().getDatabaseManager().getCaseRecords();

    public DatabaseTask(CommandSender sender) {
        super(sender);
    }

    @Override
    public void beforeProcess() throws PermissionException {
    }

    @Override
    public abstract void process() throws SQLException;

    public Dao<CaseRecord, Integer> getCaseRecords() {
        return caseRecords;
    }
}
