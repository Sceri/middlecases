package ru.middlecraft.cases.task;

import com.google.common.base.Throwables;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import ru.middlecraft.cases.MiddleCases;
import ru.middlecraft.cases.permission.PermissionException;
import ru.middlecraft.cases.util.BukkitUtils;

import java.util.logging.Level;

/**
 * Created by Sceri 23.06.2016.
 */
public abstract class Task {

    private final CommandSender sender;

    public Task(CommandSender sender) {
        this.sender = sender;
    }

    public abstract void beforeProcess() throws PermissionException;

    public abstract void process() throws Exception;

    public void handleException(Exception ex) {
        try {
            Throwables.propagateIfInstanceOf(ex, PermissionException.class);
            Throwables.propagateIfInstanceOf(ex, Exception.class);
        } catch (PermissionException e) {
            BukkitUtils.syncSendMessage(getSender(), ChatColor.RED + "У вас недостаточно прав");
        } catch (Exception e) {
            MiddleCases.logger().log(Level.WARNING, "Error while executing task", e);
        }
    }

    public CommandSender getSender() {
        return sender;
    }
}
