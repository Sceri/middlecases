package ru.middlecraft.cases.task;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Sceri 23.06.2016.
 */
public class TaskManager {
    private ExecutorService executorService = Executors.newFixedThreadPool(3);

    public void execute(final Task task) {
        try {
            task.beforeProcess();
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        task.process();
                    } catch (Exception ex) {
                        task.handleException(ex);
                    }
                }
            });
        } catch (Exception ex) {
            task.handleException(ex);
        }
    }

    public void destroy() {
        executorService.shutdown();
    }

}
