package ru.middlecraft.cases.util;

import java.lang.reflect.Array;
import java.util.List;
import java.util.Random;

/**
 * Created by Sceri 21.06.2016.
 */
public class ArrayUtils {
    private static final Random RANDOM = new Random();

    public static <T> T[] fill(T item, int length) {
        @SuppressWarnings("unchecked")
        T[] line = (T[]) Array.newInstance(item.getClass(), length);
        for(int index = 0; index < line.length; index++) {
            line[index] = item;
        }
        return line;
    }

    public static <T> T[] randomFill(List<T> items, int length) {
        @SuppressWarnings("unchecked")
        T[] line = (T[]) Array.newInstance(items.get(0).getClass(), length);
        for(int index = 0; index < line.length; index++) {
            line[index] = items.get(RANDOM.nextInt(items.size() - 1));
        }
        return line;
    }

    public static <T> T[] replace(T[] items, T item, int index) {
        items[index] = item;
        return items;
    }

    public static <T> T[] switchItems(T[] items, T firstItem, T secondItem) {
        for(int index = 0; index < items.length; index++) {
            items[index] = items[index] == firstItem ? secondItem : firstItem;
        }
        return items;
    }

}
