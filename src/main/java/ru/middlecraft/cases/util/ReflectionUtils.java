package ru.middlecraft.cases.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * Created by Sceri 28.01.2016.
 */
public class ReflectionUtils {

    public static <T> T getFieldValue(Class<?> src, String name, Class<T> type, Object from) throws SecurityException, NoSuchFieldException {
        Field field = src.getDeclaredField(name);
        field.setAccessible(true);

        try {
            return type.cast(field.get(from));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void setFieldValue(Class<?> src, String name, Object in, Object value) throws SecurityException, NoSuchFieldException {
        Field field = src.getDeclaredField(name);
        field.setAccessible(true);

        if (Modifier.isFinal(field.getModifiers())) {
            Field modifiers = Field.class.getDeclaredField("modifiers");
            modifiers.setAccessible(true);

            try {
                modifiers.set(field, field.getModifiers() & ~Modifier.FINAL);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            field.set(in, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static <T> T invokeMethod(Class<?> src, String name, Class<T> returnType, Object in, Class<?>[] args, Object... params) throws SecurityException, NoSuchMethodException {
        Method method = src.getDeclaredMethod(name, args);
        method.setAccessible(true);

        try {
            return returnType.cast(method.invoke(in, params));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static <T> T invokeConstructor(Class<T> src, Class<?>[] args, Object... params) throws SecurityException, NoSuchMethodException {
        Constructor constructor = src.getConstructor(args);

        try {
            return src.cast(constructor.newInstance(params));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static <T> T cast(Object object, Class<T> clazz) {
        return clazz.cast(object);
    }

    public static boolean classListEqual(Class<?>[] l1, Class<?>[] l2) {
        boolean equal = true;

        if (l1.length != l2.length)
            return false;
        for (int i = 0; i < l1.length; i++) {
            if (l1[i] != l2[i]) {
                equal = false;
                break;
            }
        }

        return equal;
    }

}
