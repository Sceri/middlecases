package ru.middlecraft.cases.util.json;

import com.google.gson.*;
import org.bukkit.Material;
import org.bukkit.material.MaterialData;

import java.lang.reflect.Type;

/**
 * Created by Sceri 21.06.2016.
 */
public class MaterialDataAdapter implements JsonSerializer<MaterialData>, JsonDeserializer<MaterialData> {

    public MaterialData deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();

        Material material = object.has("type") ? context.<Material>deserialize(object.get("type"), Material.class) : null;
        byte data = object.has("data") ? object.getAsJsonPrimitive("data").getAsByte() : 0;

        if(material != null) {
            return new MaterialData(material, data);
        }

        return null;
    }

    public JsonElement serialize(MaterialData material, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject object = new JsonObject();

        object.add("type", context.serialize(material.getItemType()));
        if(material.getData() != 0) {
            object.addProperty("data", material.getData());
        }

        return object;
    }
}
