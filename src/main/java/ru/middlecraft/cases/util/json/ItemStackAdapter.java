package ru.middlecraft.cases.util.json;

import com.google.common.collect.Maps;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;
import ru.middlecraft.cases.util.ItemStackBuilder;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * Created by Sceri 21.06.2016.
 */
public class ItemStackAdapter implements JsonSerializer<ItemStack>, JsonDeserializer<ItemStack> {
    private final Type loreType = new TypeToken<List<String>>() {
    }.getType();
    private final Type enchantsType = new TypeToken<Map<String, Integer>>() {
    }.getType();
    
    @Override
    public ItemStack deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();
        
        MaterialData material = object.has("material") ? context.<MaterialData>deserialize(object.get("material"), MaterialData.class) : null;
        int amount = object.has("amount") ? object.getAsJsonPrimitive("amount").getAsInt() : 1;
        String displayName = object.has("displayName") ? object.getAsJsonPrimitive("displayName").getAsString() : null;
        List<String> lore = object.has("lore") ? context.<List<String>>deserialize(object.get("lore"), loreType) : null;
        Map<String, Integer> enchants = object.has("enchants") ? context.<Map<String, Integer>>deserialize(object.get("enchants"), enchantsType) : null;

        if(displayName != null) {
            displayName = ChatColor.translateAlternateColorCodes('&', displayName);
        }
        if(lore != null) {
            for (int index = 0; index < lore.size(); index++) {
                lore.set(index, ChatColor.translateAlternateColorCodes('&', lore.get(index)));
            }
        }

        return new ItemStackBuilder(material).withAmount(amount).withName(displayName).withLore(lore).withRawNameEnchants(enchants).build();
    }

    @Override
    public JsonElement serialize(ItemStack itemStack, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject object = new JsonObject();

        object.add("material", context.serialize(itemStack.getData()));
        if(itemStack.getAmount() > 1) {
            object.addProperty("amount", itemStack.getAmount());
        }
        ItemMeta meta = itemStack.getItemMeta();
        if(meta.getDisplayName() != null) {
            object.addProperty("displayName", meta.getDisplayName());
        }
        if(meta.getLore() != null && meta.getLore().size() > 0) {
            object.add("lore", context.serialize(meta.getLore()));
        }
        if(itemStack.getEnchantments() != null && itemStack.getEnchantments().size() > 0) {
            Map<String, Integer> map = Maps.newHashMap();
            for(Map.Entry<Enchantment, Integer> entry : itemStack.getEnchantments().entrySet()) {
                map.put(entry.getKey().getName(), entry.getValue());
            }

            object.add("enchants", context.serialize(map));
        }

        return object;
    }
    
}
