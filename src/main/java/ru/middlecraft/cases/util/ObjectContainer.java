package ru.middlecraft.cases.util;

/**
 * Created by Sceri 24.06.2016.
 */
public class ObjectContainer<T> {
    private T object;

    public ObjectContainer(T object) {
        this.object = object;
    }

    public void setObject(T object) {
        this.object = object;
    }

    public T getObject() {
        return object;
    }
}
