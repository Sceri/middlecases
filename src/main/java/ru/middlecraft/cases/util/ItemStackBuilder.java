package ru.middlecraft.cases.util;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import java.util.List;
import java.util.Map;

/**
 * Created by Sceri 21.06.2016.
 */
public class ItemStackBuilder {

    private Material material;
    private short data;
    private int amount = 1;
    private String displayName;
    private List<String> lore;
    private Map<Enchantment, Integer> enchants;

    public ItemStackBuilder(Material material, short data) {
        this.material = material;
        this.data = data;
    }

    public ItemStackBuilder(Material material) {
        this(material, (short) 0);
    }

    public ItemStackBuilder(MaterialData material) {
        this(material.getItemType(), material.getData());
    }

    public ItemStackBuilder withAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public ItemStackBuilder withLore(String ... lore) {
        return withLore(Lists.newArrayList(lore));
    }

    public ItemStackBuilder withLore(List<String> lore) {
        this.lore = lore;
        return this;
    }

    public ItemStackBuilder withEnchant(int id, int level) {
        return withEnchant(Enchantment.getById(id), level);
    }

    public ItemStackBuilder withEnchant(Enchantment enchant, int level) {
        if(enchants == null) {
            enchants = Maps.newHashMap();
        }

        enchants.put(enchant, level);
        return this;
    }

    public ItemStackBuilder withEnchants(Map<Enchantment, Integer> enchants) {
        this.enchants = enchants;
        return this;
    }

    public ItemStackBuilder withRawIdEnchants(Map<Integer, Integer> enchants) {
        if(enchants != null) {
            Map<Enchantment, Integer> map = Maps.newHashMap();
            for (Map.Entry<Integer, Integer> entry : enchants.entrySet()) {
                map.put(Enchantment.getById(entry.getKey()), entry.getValue());
            }

            return withEnchants(map);
        }

        return this;
    }

    public ItemStackBuilder withRawNameEnchants(Map<String, Integer> enchants) {
        if(enchants != null) {
            Map<Enchantment, Integer> map = Maps.newHashMap();
            for (Map.Entry<String, Integer> entry : enchants.entrySet()) {
                map.put(Enchantment.getByName(entry.getKey()), entry.getValue());
            }

            return withEnchants(map);
        }

        return this;
    }

    public ItemStackBuilder withName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public ItemStack build() {
        ItemStack itemStack = new ItemStack(material, amount, data);

        ItemMeta meta = itemStack.getItemMeta();
        if(displayName != null) {
            meta.setDisplayName(displayName);
        }

        if(lore != null && lore.size() > 0) {
            meta.setLore(lore);
        }
        itemStack.setItemMeta(meta);

        if(enchants != null && enchants.size() > 0) {
            itemStack.addUnsafeEnchantments(enchants);
        }

        return itemStack;
    }
}
