package ru.middlecraft.cases.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sceri 24.06.2016.
 */
public class CollectionUtils {

    public static <V, K> Map<V, K> invertMap(Map<K, V> map) {
        Map<V, K> inv = new HashMap<>();
        for (Map.Entry<K, V> entry : map.entrySet()) {
            inv.put(entry.getValue(), entry.getKey());
        }
        return inv;
    }

}
