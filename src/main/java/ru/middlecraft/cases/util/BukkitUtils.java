package ru.middlecraft.cases.util;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import ru.middlecraft.cases.MiddleCases;

/**
 * Created by Sceri 21.06.2016.
 */
public class BukkitUtils {

    public static boolean pluginExists(String pluginName) {
        return Bukkit.getPluginManager().getPlugin(pluginName) != null;
    }

    public static void syncSendMessage(final CommandSender sender, final String message) {
        syncExecuteQuietly(new Runnable() {
            @Override
            public void run() {
                sender.sendMessage(message);
            }
        });
    }

    public static void syncExecuteQuietly(Runnable runnable) {
        try {
            syncExecute(runnable);
        } catch (Exception ignored) {
        }
    }

    public static void syncExecute(final Runnable runnable) throws Exception {
        if(MiddleCases.getInstance().getServer().isPrimaryThread()) {
            runnable.run();
        } else {
            final ObjectContainer<Exception> exception = new ObjectContainer<>(null);

            Bukkit.getScheduler().scheduleSyncDelayedTask(MiddleCases.getInstance(), new Runnable() {
                @Override
                public void run() {
                    try {
                        runnable.run();
                    } catch (Exception ex) {
                        exception.setObject(ex);
                    }
                }
            });

            if (exception.getObject() != null) {
                throw exception.getObject();
            }
        }
    }



}
