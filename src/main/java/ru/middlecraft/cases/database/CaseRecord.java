package ru.middlecraft.cases.database;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.middlecraft.cases.inventory.Case;

import java.sql.Timestamp;

/**
 * Created by Sceri 23.06.2016.
 */
@DatabaseTable(tableName = "cases")
public class CaseRecord {
    public static final String CASE_NAME_COLUMN = "name";
    public static final String CASE_PLAYER_COLUMN = "player";
    public static final String CASE_ADD_TIME_COLUMN = "add_time";
    public static final String CASE_OPEN_TIME_COLUMN = "open_time";
    public static final String CASE_REWARD_ID_COLUMN = "reward_id";
    public static final String CASE_PROCESS_TIME_COLUMN = "process_time";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(columnName = CASE_NAME_COLUMN, canBeNull = false)
    private String name;

    @DatabaseField(columnName = CASE_PLAYER_COLUMN, canBeNull = false)
    private String player;

    @DatabaseField(columnName = CASE_ADD_TIME_COLUMN)
    private Timestamp added;

    @DatabaseField(columnName = CASE_OPEN_TIME_COLUMN)
    private Timestamp opened;

    @DatabaseField(columnName = CASE_REWARD_ID_COLUMN)
    private int rewardId;

    @DatabaseField(columnName = CASE_PROCESS_TIME_COLUMN)
    private Timestamp processed;

    CaseRecord() {
        //For ORMLite
    }

    public CaseRecord(String name, String player, Timestamp added, Timestamp opened, int rewardId, Timestamp processed) {
        this.name = name;
        this.player = player;
        this.added = added;
        this.opened = opened;
        this.rewardId = rewardId;
        this.processed = processed;
    }

    public CaseRecord(String name, String player, Timestamp added) {
        this(name, player, added, null, -1, null);
    }

    public CaseRecord(Case aCase, Player player, Timestamp added) {
        this(aCase.getName(), player.getName(), added, null, -1, null);
    }

    public CaseRecord(String name, String player) {
        this(name, player, new Timestamp(System.currentTimeMillis()));
    }

    public CaseRecord(Case aCase, Player player) {
        this(aCase, player, new Timestamp(System.currentTimeMillis()));
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPlayer() {
        return player;
    }

    public Timestamp getAdded() {
        return added;
    }

    public Timestamp getOpened() {
        return opened;
    }

    public int getRewardId() {
        return rewardId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public void setAdded(Timestamp added) {
        this.added = added;
    }

    public void setOpened(Timestamp opened) {
        this.opened = opened;
    }

    public void setOpened() {
        this.opened = new Timestamp(System.currentTimeMillis());
    }

    public void setProcessed() {
        this.processed = new Timestamp(System.currentTimeMillis());
    }

    public Timestamp getProcessed() {
        return processed;
    }

    public void setRewardId(int rewardId) {
        this.rewardId = rewardId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CaseRecord)) return false;
        CaseRecord record = (CaseRecord) o;
        return getRewardId() == record.getRewardId() &&
                Objects.equal(getName(), record.getName()) &&
                Objects.equal(getPlayer(), record.getPlayer()) &&
                Objects.equal(getAdded(), record.getAdded()) &&
                Objects.equal(getOpened(), record.getOpened()) &&
                Objects.equal(getProcessed(), record.getProcessed());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getName(), getPlayer(), getAdded(), getOpened(), getRewardId(), getProcessed());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("player", player)
                .add("added", added)
                .add("opened", opened)
                .add("rewardId", rewardId)
                .add("processed", processed)
                .toString();
    }
}
