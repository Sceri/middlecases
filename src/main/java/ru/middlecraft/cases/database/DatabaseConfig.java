package ru.middlecraft.cases.database;

/**
 * Created by Sceri 23.06.2016.
 */
public class DatabaseConfig {
    private String url;
    private String userName;
    private String password;

    public String getUrl() {
        return url;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }
}
