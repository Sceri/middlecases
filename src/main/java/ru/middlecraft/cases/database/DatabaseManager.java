package ru.middlecraft.cases.database;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.logger.LocalLog;
import com.j256.ormlite.table.TableUtils;
import org.bukkit.entity.Player;
import ru.middlecraft.cases.MiddleCases;
import ru.middlecraft.cases.inventory.Case;

import java.io.File;
import java.sql.SQLException;

/**
 * Created by Sceri 23.06.2016.
 */
public class DatabaseManager {
    private JdbcPooledConnectionSource connectionSource;
    private Dao<CaseRecord, Integer> caseRecords;

    public DatabaseManager(DatabaseConfig config) throws SQLException {
        init(config);
    }

    public void init(DatabaseConfig config) throws SQLException {
        LocalLog.openLogFile(new File(MiddleCases.getInstance().getDataFolder(), "ormlite.log").getAbsolutePath());

        connectionSource = new JdbcPooledConnectionSource(config.getUrl(), config.getUserName(), config.getPassword());
        connectionSource.setTestBeforeGet(true);
        connectionSource.setMaxConnectionsFree(3);

        caseRecords = DaoManager.createDao(connectionSource, CaseRecord.class);

        if(!caseRecords.isTableExists()) {
            TableUtils.createTable(connectionSource, CaseRecord.class);
        }
    }

    public Dao<CaseRecord, Integer> getCaseRecords() {
        return caseRecords;
    }

    public void closeConnection() {
        connectionSource.closeQuietly();
    }
}
