package ru.middlecraft.cases.inventory;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import ru.middlecraft.cases.MiddleCases;
import ru.middlecraft.cases.database.CaseRecord;
import ru.middlecraft.cases.permission.PermissionException;
import ru.middlecraft.cases.inventory.item.CaseItem;
import ru.middlecraft.cases.permission.Permissions;
import ru.middlecraft.cases.task.DatabaseTask;
import ru.middlecraft.cases.util.BukkitUtils;
import ru.middlecraft.cases.util.CollectionUtils;

import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import static ru.middlecraft.cases.database.CaseRecord.*;
import static ru.middlecraft.cases.inventory.CaseInventory.WIN_INDEX;

/**
 * Created by Sceri 23.06.2016.
 */
public class Case implements Listener {

    private final String name;
    private final Map<Integer, CaseItem> itemsMap;
    private final Map<CaseItem, Integer> idMap;
    private final List<CaseItem> itemsList;
    private final double totalWeight;
    private List<Location> locations;

    public Case(String name, Map<Integer, CaseItem> itemsMap, List<Location> locations) {
        this.name = name;
        this.itemsMap = itemsMap;
        this.itemsList = Lists.newArrayList(getItemsMap().values());
        this.idMap = CollectionUtils.invertMap(itemsMap);
        this.locations = locations;
        this.totalWeight = calculateWeight();

        Bukkit.getPluginManager().registerEvents(this, MiddleCases.getInstance());
    }

    public void openCase(final Player player) {
        final CaseInventory caseInventory = createInventory();

        MiddleCases.task().execute(new DatabaseTask(player) {
            @Override
            public void beforeProcess() throws PermissionException {
                if(!getSender().hasPermission(Permissions.CASE_OPEN_COMMAND)) {
                    throw new PermissionException();
                }
            }

            @Override
            public void process() throws SQLException {
                List<CaseRecord> records = getCaseRecords().queryBuilder().
                        where().
                        eq(CASE_PLAYER_COLUMN, player.getName()).
                        and().
                        eq(CASE_NAME_COLUMN, getName()).
                        and().
                        isNull(CASE_OPEN_TIME_COLUMN).
                        query();

                if(!records.isEmpty()) {
                    CaseRecord record = Iterables.getLast(records);

                    record.setOpened();
                    record.setRewardId(getItemId(caseInventory.getWinItem()));

                    caseInventory.setRecord(record);
                    getCaseRecords().update(record);

                    BukkitUtils.syncExecuteQuietly(new Runnable() {
                        @Override
                        public void run() {
                            caseInventory.openInventory(player);
                        }
                    });
                } else {
                    BukkitUtils.syncSendMessage(getSender(), ChatColor.RED + "Нет доступных для открытия кейсов");
                }
            }
        });

    }

    public CaseInventory createInventory() {
        return new CaseInventory(this);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onItemClicked(InventoryClickEvent event) {
        Inventory inventory = event.getInventory();
        if (inventory.getHolder() instanceof CaseInventory) {
            final CaseInventory caseInventory = (CaseInventory) inventory.getHolder();
            if (event.getWhoClicked() instanceof Player) {
                final Player player = (Player) event.getWhoClicked();

                int index = event.getRawSlot();
                if (index < inventory.getSize()) {
                    if(caseInventory.isCompleted()) {
                        if(index == WIN_INDEX) {
                            MiddleCases.task().execute(new DatabaseTask(player) {
                                @Override
                                public void beforeProcess() throws PermissionException {
                                    if(!getSender().hasPermission(Permissions.CASE_GET_COMMAND)) {
                                        throw new PermissionException();
                                    }
                                }

                                @Override
                                public void process() throws SQLException {
                                    CaseRecord record = caseInventory.getRecord();
                                    if(record != null) {
                                        record.setProcessed();

                                        getCaseRecords().update(record);

                                        BukkitUtils.syncExecuteQuietly(new Runnable() {
                                            @Override
                                            public void run() {
                                                caseInventory.getWinItem().getReward().process(player);
                                                caseInventory.closeInventory(player);
                                            }
                                        });
                                    } else {
                                        BukkitUtils.syncSendMessage(getSender(), ChatColor.RED + "Ошибка при открытии кейса");
                                    }
                                }
                            });
                        }
                    }
                }
            }
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if(getLocations() != null && getLocations().size() > 0) {
            if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                Location blockLocation = event.getClickedBlock().getLocation();
                if (getLocations().contains(blockLocation)) {
                    openCase(event.getPlayer());
                    event.setCancelled(true);
                }
            }
        }
    }

    public CaseItem getRandomItem() {
        int randomIndex = -1;
        double random = Math.random() * getTotalWeight();

        for (int index = 0; index < getItemsList().size(); ++index) {
            random -= getItemsList().get(index).getWeight();
            if (random <= 0.0D) {
                randomIndex = index;
                break;
            }
        }

        return getItemsList().get(randomIndex);
    }

    private double calculateWeight() {
        double totalWeight = 0.0D;
        for(CaseItem item : getItemsList()) {
            totalWeight += item.getWeight();
        }
        return totalWeight;
    }

    public CaseItem getItem(int id) {
        if(getItemsMap().containsKey(id)) {
            return getItemsMap().get(id);
        }
        return null;
    }

    public int getItemId(CaseItem item) {
        if(getIdMap().containsKey(item)) {
            return getIdMap().get(item);
        }
        return -1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Case)) return false;
        Case aCase = (Case) o;
        return Objects.equal(getName(), aCase.getName()) &&
                Objects.equal(getItemsMap(), aCase.getItemsMap());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getName(), getItemsMap());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("itemsMap", itemsMap)
                .toString();
    }

    public String getName() {
        return name;
    }

    public Map<Integer, CaseItem> getItemsMap() {
        return itemsMap;
    }

    public List<CaseItem> getItemsList() {
        return itemsList;
    }

    private Map<CaseItem, Integer> getIdMap() {
        return idMap;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    public void addLocation(Location location) {
        locations.add(location);
    }

    public List<Location> getLocations() {
        return locations;
    }

    public double getTotalWeight() {
        return totalWeight;
    }

    public static class CaseAdapter implements JsonSerializer<Case>, JsonDeserializer<Case> {
        private static final Type ITEMS_TYPE = new TypeToken<Map<Integer, CaseItem>>() {}.getType();
        private static final Type LOCATIONS_TYPE = new TypeToken<List<Location>>() {}.getType();

        @Override
        public Case deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject object = json.getAsJsonObject();

            String name = object.has("name") ? object.get("name").getAsString() : "default";
            Map<Integer, CaseItem> itemsMap = object.has("items") ? context.<Map<Integer, CaseItem>>deserialize(object.get("items"), ITEMS_TYPE) : Maps.<Integer, CaseItem>newHashMap();
            List<Location> locations = object.has("locations") ? context.<List<Location>>deserialize(object.get("locations"), LOCATIONS_TYPE) : null;

            return new Case(name, itemsMap, locations);
        }

        @Override
        public JsonElement serialize(Case src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject object = new JsonObject();

            object.addProperty("name", src.name);
            object.add("items", context.serialize(src.itemsMap, ITEMS_TYPE));

            if(src.locations != null && src.locations.size() > 0) {
                object.add("locations", context.serialize(src.locations, LOCATIONS_TYPE));
            }

            return object;
        }
    }
}
