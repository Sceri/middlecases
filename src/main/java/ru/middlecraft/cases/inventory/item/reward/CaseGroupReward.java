package ru.middlecraft.cases.inventory.item.reward;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import org.bukkit.entity.Player;
import ru.middlecraft.cases.util.BukkitUtils;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

/**
 * Created by Sceri 21.06.2016.
 */
public class CaseGroupReward extends CaseReward {

    protected String groupName;
    protected long time;

    public CaseGroupReward(String groupName, long time) {
        super(CaseRewardType.GROUP);

        if(!BukkitUtils.pluginExists("PermissionsEx")) {
            throw new IllegalStateException("PermissionsEx should be installed");
        }

        this.groupName = groupName;
        this.time = time;
    }

    @Override
    public void process(Player player) {
        PermissionUser permissionUser = PermissionsEx.getUser(player);
        if(time != 0) {
            permissionUser.addGroup(groupName, null, time);
        } else {
            permissionUser.addGroup(groupName);
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CaseGroupReward)) return false;
        CaseGroupReward that = (CaseGroupReward) o;
        return time == that.time &&
                Objects.equal(groupName, that.groupName);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(groupName, time);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("groupName", groupName)
                .add("time", time)
                .toString();
    }
}
