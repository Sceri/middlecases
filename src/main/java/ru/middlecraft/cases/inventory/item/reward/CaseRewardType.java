package ru.middlecraft.cases.inventory.item.reward;

/**
 * Created by Sceri 21.06.2016.
 */
public enum CaseRewardType {
    ITEM, PERMISSION, GROUP
}
