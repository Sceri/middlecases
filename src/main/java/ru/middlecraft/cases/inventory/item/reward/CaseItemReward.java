package ru.middlecraft.cases.inventory.item.reward;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Sceri 21.06.2016.
 */
public class CaseItemReward extends CaseReward {

    protected ItemStack itemStack;

    public CaseItemReward(ItemStack itemStack) {
        super(CaseRewardType.ITEM);
        this.itemStack = itemStack;
    }

    @Override
    public void process(Player player) {
        player.getInventory().addItem(getItem());
    }

    public ItemStack getItem() {
        return itemStack;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CaseItemReward)) return false;
        CaseItemReward that = (CaseItemReward) o;
        return Objects.equal(itemStack, that.itemStack);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(itemStack);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("itemStack", itemStack)
                .toString();
    }
}
