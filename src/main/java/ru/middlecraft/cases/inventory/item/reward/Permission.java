package ru.middlecraft.cases.inventory.item.reward;

/**
 * Created by Sceri 22.06.2016.
 */
public class Permission {
    private String permission;
    private int time;

    public Permission(String permission, int time) {
        this.permission = permission;
        this.time = time;
    }

    public String getPermission() {
        return permission;
    }

    public int getTime() {
        return time;
    }
}
