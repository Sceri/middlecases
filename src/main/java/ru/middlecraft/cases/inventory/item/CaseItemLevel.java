package ru.middlecraft.cases.inventory.item;

/**
 * Created by Sceri 21.06.2016.
 */
public enum CaseItemLevel {
    COMMON, UNCOMMON, RARE, EPIC;
}
