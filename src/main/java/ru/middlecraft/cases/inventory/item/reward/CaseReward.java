package ru.middlecraft.cases.inventory.item.reward;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Sceri 21.06.2016.
 */
public abstract class CaseReward {

    private CaseRewardType type;

    public CaseReward(CaseRewardType type) {
        this.type = type;
    }

    public abstract void process(Player player);

    public CaseRewardType getType() {
        return type;
    }

    public static class CaseRewardAdapter implements JsonSerializer<CaseReward>, JsonDeserializer<CaseReward> {
        private final Type pexType = new TypeToken<List<Permission>>() {}.getType();

        public CaseReward deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject object = json.getAsJsonObject();

            CaseRewardType rewardType = object.has("type") ? context.<CaseRewardType>deserialize(object.get("type"), CaseRewardType.class) : null;
            if(rewardType != null) {
                switch (rewardType) {
                    case ITEM:
                        return new CaseItemReward(object.has("item") ? context.<ItemStack>deserialize(object.get("item"), ItemStack.class) : null);
                    case PERMISSION:
                        return new CasePermissionReward(object.has("permissions") ? context.<List<Permission>>deserialize(object.get("permissions"), pexType) : null);
                    case GROUP:
                        String groupName = object.has("group") ? object.getAsJsonPrimitive("group").getAsString() : null;
                        long time = object.has("time") ? object.getAsJsonPrimitive("time").getAsLong() : 0;
                        return new CaseGroupReward(groupName, time);
                    default:
                        return null;
                }
            }
            return null;
        }

        public JsonElement serialize(CaseReward reward, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject object = new JsonObject();

            object.add("type", context.serialize(reward.getType()));
            switch (reward.getType()) {
                case ITEM :
                    CaseItemReward itemReward = (CaseItemReward) reward;
                    object.add("item", context.serialize(itemReward.itemStack));
                    break;
                case PERMISSION:
                    CasePermissionReward permissionReward = (CasePermissionReward) reward;
                    object.add("permissions", context.serialize(permissionReward.permissions));
                    break;
                case GROUP:
                    CaseGroupReward groupReward = (CaseGroupReward) reward;
                    object.addProperty("group", groupReward.groupName);
                    object.addProperty("time", groupReward.time);
                    break;
            }

            return object;
        }
    }
}
