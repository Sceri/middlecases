package ru.middlecraft.cases.inventory.item;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.gson.*;
import org.bukkit.inventory.ItemStack;
import ru.middlecraft.cases.inventory.item.reward.CaseItemReward;
import ru.middlecraft.cases.inventory.item.reward.CaseReward;
import ru.middlecraft.cases.inventory.item.reward.CaseRewardType;

import java.lang.reflect.Type;

/**
 * Created by Sceri 20.06.2016.
 */
public class CaseItem {
    private ItemStack itemStack;
    private CaseItemLevel level;
    private CaseReward reward;
    private double weight;

    public CaseItem(ItemStack itemStack, CaseItemLevel level, CaseReward reward, double weight) {
        this.itemStack = itemStack;
        this.level = level;
        this.reward = reward;
        this.weight = weight;
    }

    public CaseItem(CaseItemLevel level, CaseItemReward reward, double weight) {
        this(reward.getItem(), level, reward, weight);
    }

    public CaseItemLevel getLevel() {
        return level;
    }

    public CaseReward getReward() {
        return reward;
    }

    public double getWeight() {
        return weight;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaseItem item = (CaseItem) o;
        return Double.compare(item.getWeight(), getWeight()) == 0 &&
                getLevel() == item.getLevel() &&
                Objects.equal(getReward(), item.getReward());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getLevel(), getReward(), getWeight());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("level", level)
                .add("reward", reward)
                .add("weight", weight)
                .toString();
    }

    public static class CaseItemAdapter implements JsonSerializer<CaseItem>, JsonDeserializer<CaseItem> {
        public CaseItem deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject object = json.getAsJsonObject();

            CaseItemLevel itemLevel = object.has("level") ? context.<CaseItemLevel>deserialize(object.get("level"), CaseItemLevel.class) : CaseItemLevel.COMMON;
            CaseReward reward = object.has("reward") ? context.<CaseReward>deserialize(object.get("reward"), CaseReward.class) : null;
            double weight = object.has("weight") ? object.getAsJsonPrimitive("weight").getAsDouble() : 0D;

            if(reward != null) {
                switch (reward.getType()) {
                    case ITEM:
                        return new CaseItem(itemLevel, (CaseItemReward) reward, weight);
                    default:
                        return new CaseItem(object.has("item") ? context.<ItemStack>deserialize(object.get("item"), ItemStack.class) : null, itemLevel, reward, weight);
                }
            }

            return null;
        }

        public JsonElement serialize(CaseItem item, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject object = new JsonObject();

            if(item.getLevel() != CaseItemLevel.COMMON) {
                object.add("level", context.serialize(item.level));
            }

            object.add("reward", context.serialize(item.reward));
            object.addProperty("weight", item.weight);

            if(item.getReward().getType() != CaseRewardType.ITEM) {
                object.add("item", context.serialize(item.itemStack));
            }

            return object;
        }
    }

}
