package ru.middlecraft.cases.inventory.item.reward;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import org.bukkit.entity.Player;
import ru.middlecraft.cases.util.BukkitUtils;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.util.List;

/**
 * Created by Sceri 22.06.2016.
 */
public class CasePermissionReward extends CaseReward {

    protected List<Permission> permissions;

    public CasePermissionReward(List<Permission> permissions) {
        super(CaseRewardType.PERMISSION);

        if(!BukkitUtils.pluginExists("PermissionsEx")) {
            throw new IllegalStateException("PermissionsEx should be installed");
        }

        this.permissions = permissions;
    }

    @Override
    public void process(Player player) {
        PermissionUser permissionUser = PermissionsEx.getUser(player);
        for(Permission permission : permissions) {
            if(permission.getTime() != 0) {
                permissionUser.addTimedPermission(permission.getPermission(), null, permission.getTime());
            } else {
                permissionUser.addPermission(permission.getPermission());
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CasePermissionReward)) return false;
        CasePermissionReward that = (CasePermissionReward) o;
        return Objects.equal(permissions, that.permissions);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(permissions);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("permissions", permissions)
                .toString();
    }
}
