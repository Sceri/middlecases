package ru.middlecraft.cases.inventory;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import ru.middlecraft.cases.MiddleCases;
import ru.middlecraft.cases.database.CaseRecord;
import ru.middlecraft.cases.inventory.item.CaseItem;

/**
 * Created by Sceri 20.06.2016.
 */
public class CaseInventory implements InventoryHolder {
    public static final int DEFAULT_SPEED = 20;
    public static final int DEFAULT_ACCELERATION = -1;
    public static final int ROW_LENGTH = 9;
    public static final int ITEMS_ROW = 2;
    public static final int MIDDLE_INDEX = 4;
    public static final int WIN_INDEX = ITEMS_ROW * ROW_LENGTH + MIDDLE_INDEX;
    public static final int SPIN_TIME = 100;

    private final Case box;
    private final Inventory inventory;
    private final CaseMatrix caseMatrix;

    private CaseRecord record;

    private boolean completed = false;

    private int taskId = -1;

    public CaseInventory(Case box) {
        this.box = box;
        this.inventory = Bukkit.createInventory(this, 5 * ROW_LENGTH, "Кейс");
        this.caseMatrix = new CaseMatrix(this);
    }

    public void openInventory(final Player player) {
        player.openInventory(getInventory());

        this.taskId = Bukkit.getScheduler().runTaskTimer(MiddleCases.getInstance(), new Runnable() {
            private int TICKS = 0;
            private int CURRENT_SPEED = DEFAULT_SPEED;
            private int CURRENT_ACCELERATION = DEFAULT_ACCELERATION;

            public void run() {
                if(TICKS++ % 20 == 0) {
                    CURRENT_SPEED += CURRENT_ACCELERATION;
                }

                if(TICKS % (DEFAULT_SPEED - CURRENT_SPEED) == 0) {
                    player.playSound(player.getLocation(), Sound.NOTE_PLING, 2F, 2F);
                    caseMatrix.moveLine();
                }

                if(TICKS == SPIN_TIME) {
                    Bukkit.getScheduler().cancelTask(taskId);
                    finish(player);
                }
            }
        }, 0L, 1L).getTaskId();
    }

    public void closeInventory(Player player) {
        if (getInventory().getViewers().contains(player)) {
            getInventory().getViewers().remove(player);
            player.closeInventory();
        }
    }

    public CaseItem getWinItem() {
        return getCaseMatrix().getWinItem();
    }

    public int shifts() {
        int speed = DEFAULT_SPEED;
        int acceleration = DEFAULT_ACCELERATION;

        int count = 0;
        for(int ticks = 0; ticks < SPIN_TIME; ticks++) {
            if(ticks % 20 == 0) {
                speed += acceleration;
            }

            if((ticks + 1) % (DEFAULT_SPEED - speed) == 0) {
                count++;
            }
        }

        return count;
    }

    public void setItems(int row, ItemStack ... items) {
        for(int index = 0; index < items.length; index++) {
            getInventory().setItem(row * ROW_LENGTH + index, items[index]);
        }
    }

    public void setCaseItems(int row, CaseItem ... items) {
        for(int index = 0; index < items.length; index++) {
            getInventory().setItem(row * ROW_LENGTH + index, items[index].getItemStack());
        }
    }

    public boolean isCompleted() {
        return completed;
    }

    private void finish(final Player player) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(MiddleCases.getInstance(), new Runnable() {
            @Override
            public void run() {
                getInventory().clear();
                getInventory().setItem(WIN_INDEX, getCaseMatrix().getWinItem().getItemStack());

                player.playSound(player.getLocation(), Sound.LEVEL_UP, 5F, 5F);
                completed = true;
            }
        }, 15L);
    }

    public Case getCase() {
        return box;
    }

    public CaseMatrix getCaseMatrix() {
        return caseMatrix;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public CaseRecord getRecord() {
        return record;
    }

    public void setRecord(CaseRecord record) {
        this.record = record;
    }
}
