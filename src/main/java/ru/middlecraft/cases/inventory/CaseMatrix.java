package ru.middlecraft.cases.inventory;

import com.google.common.collect.Lists;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import ru.middlecraft.cases.inventory.item.CaseItem;

import java.util.List;

import static ru.middlecraft.cases.inventory.CaseInventory.ROW_LENGTH;
import static ru.middlecraft.cases.inventory.CaseInventory.MIDDLE_INDEX;
import static ru.middlecraft.cases.util.ArrayUtils.randomFill;
import static ru.middlecraft.cases.util.ArrayUtils.replace;

/**
 * Created by Sceri 20.06.2016.
 */
public class CaseMatrix {
    private final CaseInventory inventory;
    private final CaseItem winItem;
    private CaseItem[] itemsLine;

    public CaseMatrix(CaseInventory inventory) {
        this.inventory = inventory;
        this.itemsLine = getRandomItems(winItem = getCaseInventory().getCase().getRandomItem());

        buildMatrix();
    }

    public void moveLine() {
        CaseItem last = itemsLine[itemsLine.length - 1];
        System.arraycopy(itemsLine, 0, itemsLine, 1, itemsLine.length - 1);
        itemsLine[0] = last;

        buildMatrix();
    }

    private void buildMatrix() {
        getCaseInventory().setItems(0, randomFill(GLASS_PANES, ROW_LENGTH));
        getCaseInventory().setItems(1, replace(randomFill(GLASS_PANES, ROW_LENGTH), RED_TORCH, MIDDLE_INDEX));
        getCaseInventory().setCaseItems(2, getItemsLine());
        getCaseInventory().setItems(3, replace(randomFill(GLASS_PANES, ROW_LENGTH), RED_TORCH, MIDDLE_INDEX));
        getCaseInventory().setItems(4, randomFill(GLASS_PANES, ROW_LENGTH));
    }

    private CaseItem[] getRandomItems(CaseItem winItem) {
        CaseItem[] items = new CaseItem[ROW_LENGTH];

        int shifts = getCaseInventory().shifts() % ROW_LENGTH;
        int winIndex = MIDDLE_INDEX - shifts >= 0 ? MIDDLE_INDEX - shifts : (items.length - 1) + (MIDDLE_INDEX - shifts);

        items[winIndex] = winItem;

        for (int index = 0; index < items.length; index++) {
            if (index != winIndex) {
                items[index] = getCaseInventory().getCase().getRandomItem();
            }
        }

        return items;
    }

    public CaseItem getWinItem() {
        return winItem;
    }

    public CaseItem[] getItemsLine() {
        return itemsLine;
    }

    public CaseInventory getCaseInventory() {
        return inventory;
    }

    private static final List<ItemStack> GLASS_PANES = createGlassPanes();
    private static final ItemStack RED_TORCH = new ItemStack(Material.REDSTONE_TORCH_ON);

    private static List<ItemStack> createGlassPanes() {
        List<ItemStack> stacks = Lists.newArrayList();
        for(short data = 0; data < 16; data++) {
            stacks.add(new ItemStack(Material.STAINED_GLASS_PANE, 1, data));
        }
        return stacks;
    }

}
